"""
Introduction to user functions.

See documentation: https:///
"""


def greet():
    """Prints a greet to the stdout."""
    print("Hallo daar")


def do_a_lot():
    """Does a lot (not)."""


if __name__ == "__main__":
    print("Ik het hoofdprogramma en doe wat tests.")
    greet()
