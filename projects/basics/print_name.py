import sys


def ask_age():
    age = input("Wat is je naam?")
    age = int(age)
    print("Leeftijd is", age)


def main():
    name = input("Wat is je naam?")
    # Schrijf naar de stdout
    print('Hello', name)

    message = "Don't walk on the grass"
    print(message)  # ook maar even de message


if __name__ == '__main__':
    main()
    print("Klaar")
    sys.exit(0)

