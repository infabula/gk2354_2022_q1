import json
import requests


base_url = "https://opendata.rdw.nl/resource/m9d7-ebf2.json"

def ask_numberplate():
    return "H465FX"  # fixture


def get_car_data(number_plate):
    data = None

    url = f"{base_url}?kenteken={number_plate}"
    print(url)

    response = requests.get(url)
    if response.status_code == 200:
        print("We hebben response")
        data = response.json()

    return data


def pretty_print_colour(car_data):
    car_dict = car_data[0]
    car_colour = car_dict['eerste_kleur']
    print("de kleur is.......", car_colour)


def main():
    number_plate = ask_numberplate()
    car_data = get_car_data(number_plate)
    if not car_data is None:
        pretty_print_colour(car_data)
    else:
        print("Sorry, geen resultaat.")


if __name__ == "__main__":
    main()