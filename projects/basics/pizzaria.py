import sys


menu = {42: "margarita",
        12: "peperony",
        3: "huawai",
        13: "calzone",
        5: "quatro stagione",
        }


def make_dow():
    pass  # TODO


def order_pizza(money, order_number=42):
    """
    parameters: money,  float, verplicht
                order_number: int, niet verplicht
    :return: pizza en eventueel bonnetje
    """
    print("Uw order wordt verwekt, nummer ", order_number, "en money", money)
    pizza_price = 7.00
    if pizza_price <= money:
        print("Voldoende geld.")
        """"""
        if order_number in menu:
            print("Oleee, we eten vandaag variabele pizza")
            pizza_name = menu[order_number]
            return pizza_name

        else:
            print("Pizza bestaat niet.")
            sys.exit(42)


        return "margarita"  # fixture
    else:
        #  exceptionele toestand
        print("Te weinig geld.")
        sys.exit(20)


def get_menu():
    """returns menu dictionary"""
    return menu

def print_menu():
    """Print the menu line by line"""
    print("Het menu")
    for number, name in menu.items():
        line = f"- {number:^6} {name:<20}"
        print(line)


def ask_pizza_number():
    """vraag interactief naar een nummer
    - moet een integer zijn : int()
    - bij een negatief getal, return 42
    - test of het nummer past bij een pizza-naam
    - als het getal niet herkend wordt, print een bericht en return 42
    """
    while True:
        number = int(input("Welke nummer pizza wens je?"))

        if number <= 0:
            print("Het getal moet positief zijn.")
        elif number == 12:
            print("peperony")
            return 12
        elif number == 3:
            print("huaway")
            return 3
        else:
            print("Onbekende keuze")



def choose_pizza():
    """
    :return:
    """
    print_menu()
    order_number = ask_pizza_number()
    return order_number


def eat_pizza(pizza):
    print("Lekker, we eten", pizza)


def main():
    money_in_pocket = 8.50
    pizza_number = choose_pizza()
    pizza = order_pizza(money=money_in_pocket,
                        order_number=pizza_number)
    eat_pizza(pizza)


if __name__ == "__main__":
    main()