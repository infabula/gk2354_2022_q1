def print_stars_line_length_twenty():
    """Print een horizontale lijn van 10 sterren."""
    fixed_line = '*' * 20
    print(fixed_line)


def print_stars_line(star_length):
    """Print een horizontale lijn van `length` aantal sterren."""
    variable_line = '*' * star_length
    print(variable_line)


def print_line(line_length, character):
    '''Print een horizontale lijn van length aantal characters. '''
    char_line = character * line_length
    print(char_line)


def main():
    main_length = 25

    print_stars_line_length_twenty()
    print_stars_line(star_length=main_length)
    print_stars_line(star_length=main_length + 5)

    char = '#'  # literal
    print_line(line_length=main_length, character="%")
    print_line(line_length=main_length, character=char)


if __name__ == "__main__":
    main()