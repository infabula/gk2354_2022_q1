import json


def write_to_file(filename, cities):
    """ open de filename
    voor elke stad, schrijf als regel naar de file
    klaar! Neeeeeee, sluit de file
    """
    outfile = open(filename, 'w')
    for city in cities:
        outfile.write(city + "\n")
        #  print(city, file=outfile)
    outfile.close()


def read_from_file(filename):
    cities = []
    """ open de file 
    lees regel voor regel de stad in 
      en kuis de stad
      en plaats in de cities-list
    tot slot, sluit de file
    """
    with open(filename, 'r') as infile:
        for line in infile:
            city = line.strip()
            if not city == '':
                cities.append(city)
        # file wordt automatisch gesloten

    return cities


def write_as_json(filename, cities):
    with open(filename, 'w') as outfile:
        cities_as_json = json.dumps(cities, indent=2)
        outfile.write(cities_as_json)


def add_more(cities):
    """
    - voeg Helsinki toe aan het einde van de lijst
    - voeg ~Madrid~ toe tussen ~Tokyo~ en ~Paris~
    """
    cities.append("Helsinki")
    paris_index = cities.index('Paris')
    cities.insert(paris_index, 'Madrid')

def get_five_character_cities(cities, test=False):
    five_character_names = []
    '''Doorloop de list servers. 
       indien de lengte van de plaatsnaam lengte 5 heeft, 
       voeg dan toe aan five_character_names'''

    for city in cities:
        if len(city) == 5:
            five_character_names.append(city)
            if test:
                print(city, " voegen toe")
        else:
            if test:
                print(city, " laten we voor wat het is")

    return five_character_names


def main():
    filename = "cities.txt"
    cities = read_from_file(filename)
    #write_to_file(filename, cities)
    print(cities)

    add_more(cities)
    print(cities)

    filtered = get_five_character_cities(cities, test=True)
    print(filtered)


if __name__ == "__main__":
    main()