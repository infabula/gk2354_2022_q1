import json


menu = {42: "margarita",
        12: "peperony",
        3: "huawai",
        13: "calzone",
        5: "quatro stagione",
        }

person1 = {"first_name": "Bob",
           "last_name": "Bouwer",
           'age': 42,
          }

person2 = {"first_name": "Eva",
           "last_name": "Elan",
           "age": 27,
           }

print(person1)
person1['age'] += 1
print(person1)

pizza = input("Welke zal ik zoeken?")

for pizza_nummer, pizza_name in menu.items():
    #print(pizza_nummer, pizza_name)
    if pizza_name == pizza.strip().lower():
        print("gevonden, nummer", pizza_nummer)
        break

json_menu = json.dumps(menu, indent=2)
outfile = open('menu.json', 'w')
outfile.write(json_menu)
outfile.close()




"""
op de interactieve terminal

- print menu
- vraag de waarde van pizzanummer 13
- voeg 

"""