def get_current_day(language):
    """TODO: uitwerken"""
    return "dinsdag"  # fixture


today = get_current_day(language="nl")  # globale variable
test_modus = False

def add(first, second):
    total = first + second
    return total


def ask_user_for_number(test=False):
    """TODO: maak interactief"""
    if test:
        return 4

    value = int(input("Waarde:"))
    return value


def main():
    num1 = ask_user_for_number(test=test_modus) # local variable
    num2 = ask_user_for_number(test=test_modus)  # local variable
    som = add(first=num1,
              second=num2)  # local variable
    print("Het is vandaag", today)
    print("De optelling resulteert in ", som)


if __name__ == "__main__":
    main()

