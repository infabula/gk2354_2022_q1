def get_names():
    names = ['Jan', 'Pier', 'Tjoris', 'Korneel']  # fixture
    return names


def print_names(names):
    """print eerst hoeveel namen er in totaal zijn
    print vervolgens elke naam per regel
    """
    count = len(names)
    header = f"Er zijn {count} namen."
    print(header)
    for index, name in enumerate(names):
        print(index, name)


def main():
    names = get_names()
    print_names(names)


if __name__ == "__main__":
    main()